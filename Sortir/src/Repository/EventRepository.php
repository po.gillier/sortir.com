<?php

namespace App\Repository;

use App\Entity\Event;
use App\Entity\Site;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Event|null find($id, $lockMode = null, $lockVersion = null)
 * @method Event|null findOneBy(array $criteria, array $orderBy = null)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Event::class);
    }

    /**
     * Renvois la liste des events pour l'affichage en HomePage
     */
    public function GetAllEventWithOrganizerAndCurrentUser(EntityManagerInterface $em) {
        $events = $this->findAll();
        foreach ($events as $event) {
            $participants = $event->getUsers();
            $event->setUsers($participants);
        }
        return $events;
    }

    /**
     * Renvoit la liste des events où l'utilisateur connecté n'est pas inscrit
     */
    public function GetEventNotRegister(EntityManagerInterface $em, $id){
        $userWeWant = $em->getRepository(User::class)->find($id);
        $events = $em->getRepository(Event::class)->findAll();
        $eventNotRegister= [];

        foreach ($events as $event){
            $usersCollection = $event->getUsers();
            $usersArray = $usersCollection->getValues();
            $userParticipateInEvent = in_array( $userWeWant , $usersArray);

            if($userParticipateInEvent == false){
                $eventNotRegister[] = $event;
            }
        }

        return $eventNotRegister;
    }

    /**
     * Renvoit la liste des events passés
     */
    public function GetEventPast(EntityManagerInterface $em){
        $events = $em->getRepository(Event::class)->findAll();
        $dateNow = new \DateTime("now");
        $eventPast = [];

        foreach ($events as $event) {
            $eventStartDate = $event->getStartDatetime();
            $eventStartDateTime = $eventStartDate->getTimestamp();
            $eventDuration = $event->getDuration();
            $eventEndDateTime = $eventStartDateTime + ($eventDuration * 60);
            $eventEndDate = date("Y-m-d H:i:s", $eventEndDateTime);

            if($eventEndDate < $dateNow)
            {
                $eventPast[] = $event;
            }

            return $eventPast;
        }
    }

    /**
     * Renvoit la liste des events par nom
     */
    public function GetEventByName(EntityManagerInterface $em, $name){
        $events = $em->getRepository(Event::class)->findAll();
        $eventsByName = [];

        foreach ($events as $event) {
            $eventName = strtolower($event->getName());
            $nameLower = strtolower($name);

            if (strpos($eventName, $nameLower) !== false) {
                $eventsByName [] = $event;
            }
        }

        return $eventsByName;
    }

    /**
     * Renvoit la liste des events triés par site
     */
    public function GetEventBySite(EntityManagerInterface $em, $idSite){
        $events = $em->getRepository(Event::class)->findBy(['site' => $idSite]);
        return $events;

    }

    /**
     * Renvoit la listye des events triés par dates
     */
    public function GetEventByDate($em, $dateSince, $dateUntil){
        $qb = $this->createQueryBuilder('e');
        $qb->where('e.startDatetime BETWEEN :since AND :until' )
            ->setParameter('since', $dateSince)
            ->setParameter('until', $dateUntil);

        $query = $qb->getQuery();
        $result = $query->getResult();

        return $result;
    }
}
