<?php

namespace App\Controller;

use App\Entity\City;
use App\Entity\Event;
use App\Entity\Group;
use App\Entity\Place;
use App\Entity\Site;
use App\Entity\State;
use App\Entity\User;
use App\Form\CancelEventType;
use App\Form\EventType;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/event")
 */
class EventController extends AbstractController
{
    /**
     * @Route("/", name="event")
     */
    public function index()
    {
        return $this->render('event/index.html.twig', [
            'controller_name' => 'EventController',
        ]);
    }

    /**
     * @Route("/cancel/{id}", name="event_cancel")
     */
    public function cancel(EntityManagerInterface $em, Request $request, string $id = null)
    {
        $event = $em->getRepository(Event::class)->find($id);
        $event->setDescription('');

        $form = $this->createForm(CancelEventType::class, $event);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $curDate = new \DateTime('midnight');
            if ($event->getStartDatetime() > $curDate) {

                $state = $em->getRepository(State::class)->findOneBy(['strState' => 'Cancelled']);
                $event->setState($state);

                $em->persist($event);
                $em->flush();
            }

            return $this->redirectToRoute('home');
        }

        return $this->render(
            "event/cancel.html.twig",
            [
                "form" => $form->createView(),
                "event" => $event
            ]
        );
    }

    /**
     * @Route("/leave/{id}", name="event_leave")
     */
    public function leave(EntityManagerInterface $em, string $id = null)
    {
        $event = $em->getRepository(Event::class)->find($id);

        $curDate = new \DateTime;
        if ($event->getStartDatetime() > $curDate) {
            $event->removeUser($this->getUser());

            $em->persist($event);
            $em->flush();
        }

        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/join/{id}", name="event_join")
     */
    public function join(EntityManagerInterface $em, string $id = null)
    {
        $event = $em->getRepository(Event::class)->find($id);

        $curDate = new \DateTime("midnight");

        if ($event->getState()->getStrState() == 'Open' && $event->getRegistrationDeadline() >= $curDate) {
            $event->addUser($this->getUser());
            $em->persist($event);
            $em->flush();
        }

        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/published/{id}", name="event_published")
     */
    public function published(EntityManagerInterface $em, string $id = null)
    {
        $event = $em->getRepository(Event::class)->find($id);
        $state = $em->getRepository(State::class)->findOneBy(array('strState' => 'Open'));
        $event->setState($state);
        $em->persist($event);
        $em->flush();

        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/detail/{id}", name="event_detail")
     */
    public function detail(EntityManagerInterface $em, Request $request, string $id)
    {
        $event = $em->getRepository(Event::class)->find($id);

        $user = $em->getRepository(User::class)->find(3);

        $groups = $em->getRepository(Group::class)->getWithUsers($event->getOrganizer()->getId());

        if ($event == null) {
            return $this->redirectToRoute('home');
        }

        return $this->render(
            "event/detail.html.twig",
            [
                "id" => $id,
                "event" => $event,
                "orgaGroups" => $groups
            ]
        );
    }

    public function formValidator(Form $form)
    {
        if ($form['newCity']->getData() == '' && $form['city']->getData() == '') {
            $form->get('city')->addError(new FormError("City or new city is mandatory"));
        }
        if ($form['placeSelect']->getData() == 0) {
            if ($form->getData()->getPlace()->getName() == '' || $form->getData()->getPlace()->getStreet() == '') {
                $form->get('placeSelect')->addError(new FormError("Place or new place is mandatory"));
            }
        }

        $curDate = new \DateTime();
        if ($form->get('startDatetime')->getData() < $curDate) {
            $form->get('startDatetime')->addError(new FormError("Start date can't be earlier than current date"));
        }
        if ($form->get('registrationDeadline')->getData() > $form->get('startDatetime')->getData()) {
            $form->get('registrationDeadline')->addError(new FormError("Registration deadline must be earlier than start date"));
        }
        if ($form->get('maxUserNumber')->getData() < 1) {
            $form->get('maxUserNumber')->addError(new FormError("Minimum: 1"));
        }
        if ($form->get('duration')->getData() < 1) {
            $form->get('duration')->addError(new FormError("Minimum: 1"));
        }
    }

    public function saveEvent(EntityManagerInterface $em, Form $form, Event $event){
        if ($form->get('save')->isClicked()) {
            $event->setState($em->getRepository(State::class)->findOneBy(['strState' => 'Created']));
        } else if ($form->get('publish')->isClicked()) {
            $event->setState($em->getRepository(State::class)->findOneBy(['strState' => 'Open']));
        }

        $place = new Place();
        if ($form['newCity']->getData() <> '') {
            $city = new City();
            $city->setName($form['newCity']->getData());
            $city->setPostCode($form->get('cityType')->get('postCode')->getData());
            $em->persist($city);
        }

        if ($form['placeSelect']->getData() == 0) {
            $place->setName($form->getData()->getPlace()->getName());
            $place->setStreet($form->getData()->getPlace()->getStreet());
            $place->setLatitude($form->getData()->getPlace()->getLatitude());
            $place->setLongitude($form->getData()->getPlace()->getLongitude());

            if ($form['newCity']->getData() <> '') {
                $place->setCity($city);
            } else {
                $place->setCity($form['city']->getData());
            }


            $em->persist($place);
        } else {
            $place = $em->getRepository(Place::class)->find($form['placeSelect']->getData());
        }
        $event->setPlace($place);

        $event->setOrganizer($this->getUser());

        $em->persist($event);
        $em->flush();
    }

    /**
     * @Route("/create", name="event_create")
     */
    public function create(EntityManagerInterface $em, Request $request)
    {
        $event = new Event();

        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $this->formValidator($form);

            if ($form->isValid()) {
                $this->saveEvent($em, $form, $event);

                return $this->redirectToRoute('home');
            }
        }

        return $this->render(
            "event/create.html.twig",
            [
                "createForm" => $form->createView(),
                "event" => $event,
                "isUpdate" => false,
            ]
        );
    }

    /**
     * @Route("/update/{id}", name="event_update")
     */
    public function update(EntityManagerInterface $em, Request $request, string $id)
    {
        $event = $em->getRepository(Event::class)->find($id);

        if ($event == null) {
            return $this->redirectToRoute('home');
        }

        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $this->formValidator($form);

            if ($form->isValid()) {
                $this->saveEvent($em, $form, $event);
            }
        }

        return $this->render(
            "event/create.html.twig",
            [
                "createForm" => $form->createView(),
                "event" => $event,

                "isUpdate" => true,
            ]
        );
    }

    /**
     * @Route("/toast/", name="places_by_city")
     */
    public function listByCity(EntityManagerInterface $em, Request $req)
    {
        $name = $req->getContent();
        $city = $em->getRepository(City::class)->findOneBy(['name' => $name]);

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($city, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonContent, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("/delete/{id}", name="event_delete")
     */
    public function delete(EntityManagerInterface $em, Request $req, string $id)
    {
        $event = $em->getRepository(Event::class)->find($id);

        $em->remove($event);
        $em->flush();

        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/GetListEvent/{id}", name="GetListEvent")
     */
    public function getListEvent(EntityManagerInterface $em, SerializerInterface $serializer, $id = null)
    {

        $eventRepository = $em->getRepository(Event::class);
        $events = $eventRepository->createQueryBuilder('e')
            ->leftJoin('e.site', 'site')->addSelect('site')
            ->leftJoin('e.organizer', 'organizer')->addSelect('organizer')
            ->leftJoin('e.users', 'users')->addSelect('users')
            ->leftJoin('users.events', 'events')->addSelect('events')
            ->leftJoin('e.state', 'state')->addSelect('state')->getQuery()->getArrayResult();

        $user = $em->getRepository(User::class)->find($id);
        $myEvent = [];
        foreach ($events as $event) {
            $curDate = new \DateTime();
            $curEvent = new Event();
            if($event["state"]["strState"] == 'Open' && $curDate > $event["startDatetime"]){
                $curEvent = $em->getRepository(Event::class)->find($event['id']);
                $state = $em->getRepository(State::class)->findOneBy(['strState'=>'In progress']);
                $curEvent->setState($state);
                $event["state"]["strState"] = 'In progress';
            }

            $minutesToAdd = $event['duration'];
            $endDate = $event['startDatetime']->add(new \DateInterval('PT' . $minutesToAdd . 'M'));
            
            if($event["state"]['strState'] == "In progress" && $curDate > $endDate){
                $curEvent = $em->getRepository(Event::class)->find($event['id']);
                $state = $em->getRepository(State::class)->findOneBy(['strState'=>'Past']);
                $curEvent->setState($state);
                $event["state"]["strState"] = 'Past';
            }

            if($curEvent->getName() <> ''){
                $em->persist($curEvent);
                $em->flush();
            }

            if ($event["state"]["strState"] != "Created") {
                $myEvent[] = $event;
            } elseif ($event["state"]["strState"] == "Created" && $event["organizer"]["id"] == $user->getId()) {
                $myEvent[] = $event;
            }
        }

        $jsonContent = $serializer->serialize($myEvent, 'json', [
            'attributes' =>
                [
                    'id',
                    'name',
                    'startDatetime',
                    'duration',
                    'registrationDeadline',
                    'maxUserNumber',
                    'description',
                    'site' => [
                        'id',
                        'name'
                    ],
                    'organizer' => [
                        'id'
                    ],
                    'users' => [
                        'id',
                        'events' => ['id']
                    ],
                    'state'
                ],
            'Content-type' => 'application/json',
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);
        return JsonResponse::fromJsonString($jsonContent);
    }

    /**
     * @Route("/GetUserById/{id}", name="GetUserById")
     */
    public function getUserById(EntityManagerInterface $em, SerializerInterface $serializer, $id = null)
    {
        $userRepository = $em->getRepository(User::class);
        $user = $userRepository->createQueryBuilder('u')
            ->leftJoin('u.events', 'events')->addSelect('events')
            ->leftJoin('events.state', 'state')->addSelect('state')
            ->leftJoin('events.organizer', 'organizer')->addSelect('organizer')
            ->leftJoin('events.users', 'users')->addSelect('users')
            ->leftJoin('u.myEvents', 'myEvents')->addSelect('myEvents')
            ->where('u.id = ?1')
            ->setParameter(1, $id)
            ->getQuery()->getArrayResult();

        $jsonContent = $serializer->serialize($user, 'json', [
            'Content-type' => 'application/json',
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);
        return JsonResponse::fromJsonString($jsonContent);

    }

    /**
     * @Route("/GetEventById/{id}", name="GetEventById")
     */
    public function getEventById(EntityManagerInterface $em, $id = null)
    {
        $eventRepository = $em->getRepository(Event::class);
        $event = $eventRepository->find($id);
        $encoders = [new XmlEncoder(), new JsonEncoder()];

        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($event, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonContent, 200, ['Content-Type' => 'application/json', AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true]);
    }


    /**
     * @Route("/GetEventByOrganizer/{id}", name="GetEventByOrganizer")
     */
    function getEventsByOrganizer(EntityManagerInterface $em, $id = null)
    {
        $eventRepository = $em->getRepository(Event::class);
        $event = $eventRepository->findBy(["organizer" => "$id"]);
        $encoders = [new XmlEncoder(), new JsonEncoder()];

        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($event, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonContent, 200, ['Content-Type' => 'application/json', AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true]);
    }

    /**
     * @Route("/GetEventRegisterById/{id}", name="GetEventRegisterById")
     */
    function getEventRegisterById(EntityManagerInterface $em, $id = null)
    {

        /** @var User $user */
        $userRepository = $em->getRepository(User::class);
        $user = $userRepository->find($id);
        $event = $user->getEvents();
        $encoders = [new XmlEncoder(), new JsonEncoder()];

        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($event, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonContent, 200, ['Content-Type' => 'application/json', AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true]);
    }

    /**
     * @Route("/GetEventNotRegisterById/{id}", name="GetEventNotRegisterById")
     */
    function getEventNotRegisterById(EntityManagerInterface $em, $id = null)
    {
        $eventRepository = $em->getRepository(Event::class);
        $eventNotRegister = $eventRepository->GetEventNotRegister($em, $id);

        $encoders = [new XmlEncoder(), new JsonEncoder()];

        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($eventNotRegister, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonContent, 200, ['Content-Type' => 'application/json', AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true]);
    }

    /**
     * @Route("/GetEventPast", name="GetEventPast")
     */
    function getEventPast(EntityManagerInterface $em)
    {
        $eventRepository = $em->getRepository(Event::class);
        $eventPast = $eventRepository->GetEventPast($em);

        $encoders = [new XmlEncoder(), new JsonEncoder()];

        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($eventPast, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonContent, 200, ['Content-Type' => 'application/json', AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true]);
    }

    /**
     * @Route("/GetListSite", name="GetListSite")
     */
    function getListSite(EntityManagerInterface $em)
    {
        $siteRepository = $em->getRepository(Site::class);
        $sites = $siteRepository->findAll();
        $encoders = [new XmlEncoder(), new JsonEncoder()];

        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($sites, 'json', [
            'attributes' => ['id', 'name', 'events' => ['id', 'users' => ['id', 'events']]],
            'circular_reference_handler' => function ($object) {
                return null;
            }
        ]);

        return new Response($jsonContent, 200, ['Content-Type' => 'application/json', AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true]);
    }

    /**
     * @Route("/GetEventByName/{name}", name="GetEventByName")
     */
    function getEventByName(EntityManagerInterface $em, $name = null)
    {
        $eventRepository = $em->getRepository(Event::class);
        $eventByName = $eventRepository->GetEventByName($em, $name);

        $encoders = [new XmlEncoder(), new JsonEncoder()];

        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($eventByName, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonContent, 200, ['Content-Type' => 'application/json', AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true]);
    }

    /**
     * @Route("/GetEventBySite/{idSite}", name="GetEventBySite")
     */
    function getEventBySite(EntityManagerInterface $em, int $idSite = null)
    {
        $eventRepository = $em->getRepository(Event::class);
        $eventBySite = $eventRepository->GetEventBySite($em, $idSite);

        $encoders = [new XmlEncoder(), new JsonEncoder()];

        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($eventBySite, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonContent, 200, ['Content-Type' => 'application/json', AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true]);
    }

    /**
     * @Route("/GetEventByDate/{dateSince}/{dateUntil}", name="GetEventByDate")
     */
    function getEventByDate(EntityManagerInterface $em, $dateSince = null, $dateUntil = null)
    {
        $eventRepository = $em->getRepository(Event::class);
        $eventBySite = $eventRepository->GetEventByDate($em, $dateSince, $dateUntil);

        $encoders = [new XmlEncoder(), new JsonEncoder()];

        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($eventBySite, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonContent, 200, ['Content-Type' => 'application/json', AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true]);
    }


}
