<?php


namespace App\Controller;

use App\Entity\Event;
use App\Entity\Site;
use App\Entity\State;
use App\Entity\User;
use App\Form\CreateUserType;
use App\Form\CsvUploadType;
use App\Form\UserType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use PhpParser\Node\Expr\Cast\Object_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminController extends EasyAdminController
{

    /**
     * @Route("/admin/desactive/{$id}", name="user_upload")
     */
    public function desactive(EntityManagerInterface $em, Request $request, string $id) {
        $user = $em->getRepository(User::class)->find($id);

        $user->setIsActive(false);
        $em->persist($user);
        $em->flush();
    }

    /**
     * @Route("/admin/cancelEvent/{id}", name="admin_cancel_event")
     */
    public function cancelEvent(Request $request, string $id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository(Event::class)->find($id);

        $curDate = new \DateTime('midnight');
        if($event->getStartDatetime() > $curDate){
            $state = $em->getRepository(State::class)->findOneBy(['strState'=>'Cancelled']);
            $event->setState($state);

            $em->persist($event);
            $em->flush();
        }

        return $this->redirectToRoute('easyadmin', array(
            'action' => 'list',
            'entity' => $event,
        ));
    }


    /**
     * @Route("/admin/userUpload", name="user_upload")
     */
    public function getUploadCSVForm(EntityManagerInterface $em, Request $request, UserPasswordEncoderInterface $userPasswordEncoder) {
        $form = $this->createForm(CsvUploadType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $csvFile = $form->get('csv')->getData();
            $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);
            $csvData = $serializer->decode(file_get_contents($csvFile), 'csv');
            $users = [];
            $repSite = $em->getRepository(Site::class);

            foreach ($csvData as &$ligne) {
                $user = new User();
                $user->setSite($repSite->find($ligne['site_id']));
                $user->setNickname($ligne['nickname']);
                $user->setPassword($ligne['password']);
                $password = $userPasswordEncoder->encodePassword($user, $user->getPassword());
                $user->setPassword($password);
                $user->setName($ligne['name']);
                $user->setFirstname($ligne['firstname']);
                $user->setPhoneNumber($ligne['phone_number']);
                $user->setMail($ligne['mail']);
                $user->setIsAdmin($ligne['is_admin']);
                $user->setIsActive($ligne['is_active']);
                $users[] = $user;
            }

            foreach ($users as &$u) {
                $em->persist($u);
            }
            $em->flush();

            return $this->redirectToRoute('easyadmin', array(
                'action' => 'list',
                'entity' => 'User',
            ));
        }

        return $this->render('admin/admin.html.twig', [
            'uploadCSVForm' => $form->createView()
        ]);
    }

    protected function persistUserEntity($user)
    {
        $encodedPassword = $this->encodePassword($user, $user->getPassword());
        $user->setPassword($encodedPassword);

        parent::persistEntity($user);
    }

    protected function updateUserEntity($user)
    {
        $encodedPassword = $this->encodePassword($user, $user->getPassword());
        $user->setPassword($encodedPassword);

        parent::updateEntity($user);
    }

    private function encodePassword($user, $password)
    {
        $passwordEncoderFactory = new EncoderFactory([
            User::class => new MessageDigestPasswordEncoder('sha512', true, 5000)
        ]);

        $encoder = $passwordEncoderFactory->getEncoder($user);

        return $encoder->encodePassword($password, $user->getSalt());
    }




}