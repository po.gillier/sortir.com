<?php


namespace App\Controller;

use App\Entity\Site;
use App\Entity\User;
use App\Form\ForgottenPasswordType;
use App\Form\ResetPasswordType;
use App\Form\UserType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PhpParser\Node\Expr\Cast\Object_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class profileController extends AbstractController
{

    /**
     * @Route("/profile/{id}", name="profile")
     */
    public function profile(EntityManagerInterface $em, Request $request, string $id){
        $user = $em->getRepository(User::class)->find($id);

        return $this->render('user/profile.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/myprofile", name="myprofile")
     * @param string $id
     * @param EntityManagerInterface $em
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function myProfile(EntityManagerInterface $em, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $rep = $em->getRepository(User::class);
        $user = $rep->find($this->getUser()->getId());
        $previousAvatarFileName = $user->getAvatarFileName();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        $oldPassword = $form->get('oldPassword')->getData();

        if ($form->isSubmitted() && ($oldPassword || $form->get("newPassword")->getData())) {
            if (!$passwordEncoder->isPasswordValid($user, $oldPassword)) {
                $form->get('oldPassword')->addError(new FormError('Mauvais mot de passe'));
            }
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $avatarFile = $form->get('avatar')->getData();
            if ($avatarFile) {
                $originalFilename = pathinfo($avatarFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$avatarFile->guessExtension();
                // Move the file to the directory where brochures are stored
                try {
                    if($previousAvatarFileName){
                        $filesystem = new Filesystem();
                        $filesystem->remove($this->getParameter('avatar_directory').$previousAvatarFileName);

                    }
                    $avatarFile->move(
                        $this->getParameter('avatar_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    dump(e);
                }
                $user->setAvatarFileName($newFilename);
            }

            $user = $form->getData();
            $newPassword = $form->get("newPassword")->getData();

            if ($oldPassword && $newPassword) {
                $password = $passwordEncoder->encodePassword($user, $user->getPassword());
                $user->setPassword($password);
            }

            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('myprofile', ['id' => $user->getId()]);
        }
        return $this->render('myProfile.html.twig', [
            'profileForm' => $form->createView(),
            'avatarFileName' =>
            $previousAvatarFileName
        ]);
    }


    /**
     * @Route("/forgotpassword/", name="forgotpassword")
     */
    public function forgotpassword(EntityManagerInterface $em, Request $request){

        $token =null;
        $rep = $em->getRepository(User::class);
        $userForm  = new User();
        $form = $this->createForm(ForgottenPasswordType::class, $userForm);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $userForm = $form->getData();
            $tabUser = $rep->findBy(['nickname' => $userForm->getNickname()]);
            if($tabUser && count($tabUser) == 1) {
                $token = rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');

                $user = $tabUser[0];
                $user->setToken($token);
                $em->flush();
            }
        }
        return $this->render('user/forgottenPassword.html.twig', [
            'forgottenPassword' => $form->createView(),
            'token' => $token
        ]);
    }
    /**
     * @Route("/forgotpassword/resetpassword/{token}", name="resetpassword")
     */
    public function resetPassword(EntityManagerInterface $em, Request $request, UserPasswordEncoderInterface $passwordEncoder, string $token){
        $rep = $em->getRepository(User::class);
        $user = new User();
        $form = $this->createForm(ResetPasswordType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $tabUser = $rep->findBy(['token' => $token]);
            if($tabUser && count($tabUser) == 1) {
                $resetUser = $tabUser[0];
                $newPassword = $form->get("newPassword")->getData();
                $password = $passwordEncoder->encodePassword($user, $newPassword);
                $resetUser->setPassword($password);
                $em->flush();
                return $this->redirectToRoute('home');
            }
        }

        return $this->render('user/ResetPassword.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}