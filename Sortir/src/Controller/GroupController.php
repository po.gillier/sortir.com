<?php

namespace App\Controller;

use App\Entity\Group;
use App\Form\GroupType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/group")
 */
class GroupController extends AbstractController
{
    /**
     * @Route("/create", name="group_create")
     */
    public function create(EntityManagerInterface $em, Request $request)
    {
        $group = new Group();
        $group->setOwner($this->getUser());

        $form = $this->createForm(GroupType::class, $group);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $em->persist($group);
            $em->flush();

            return $this->redirectToRoute('group_list');
        }

        return $this->render(
            "group/create.html.twig",
            [
                "createForm" => $form->createView(),
                "group" => $group,
            ]
        );
    }

    /**
     * @Route("/update/{id}", name="group_update")
     */
    public function update(EntityManagerInterface $em, Request $request, string $id)
    {
        $group = $em->getRepository(Group::class)->find($id);

        $form = $this->createForm(GroupType::class, $group);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($group);
            $em->flush();

            return $this->redirectToRoute('group_list');
        }

        return $this->render(
            "group/create.html.twig",
            [
                "createForm" => $form->createView(),
                "group" => $group,
            ]
        );
    }

    /**
     * @Route("/delete/{id}", name="group_delete")
     */
    public function delete(EntityManagerInterface $em, Request $request, string $id){
       $group =  $em->getRepository(Group::class)->find($id);

       $em->remove($group);
       $em->flush();

       return $this->redirectToRoute('group_list');
    }

    /**
     * @Route("/list", name="group_list")
     */
    public function list(EntityManagerInterface $em)
    {
        $groups = $em->getRepository(Group::class)->findBy(['owner'=>$this->getUser()->getId()]);

        return $this->render('group/index.html.twig', [
            'groups' => $groups,
        ]);
    }
}
