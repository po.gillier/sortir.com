<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CreateUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nickname',
                TextType::class, array('attr' => array('required' => true)))
            ->add('password',
                TextType::class, array('attr' => array('required' => true)))
            ->add('name',
                TextType::class, array('attr' => array('required' => true)))
            ->add('firstname',
                TextType::class, array('attr' => array('required' => true)))
            ->add('phoneNumber',
                TextType::class, array('attr' => array('required' => true)))
            ->add('mail',
                TextType::class, array('attr' => array('required' => true)))
            ->add('isAdmin')
            ->add('isActive')
            ->add('site')
            ->add('save', SubmitType::class, [
                'label' => 'Add user'])


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
