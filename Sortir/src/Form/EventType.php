<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Event;
use App\Entity\Place;
use App\Entity\Site;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Event\PostSetDataEvent;
use Symfony\Component\Form\Event\PreSetDataEvent;
use Symfony\Component\Form\Event\PreSubmitEvent;
use Symfony\Component\Form\Event\SubmitEvent;
use Symfony\Component\Form\Extension\Core\Type\DateIntervalType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('startDatetime', DateTimeType::class, [
                'date_widget' => 'single_text',
                'time_widget' => 'single_text'
            ])
            ->add('duration')
            ->add('registrationDeadline',DateType::class, [
                'widget' => 'single_text'
            ])
            ->add('maxUserNumber')
            ->add('description')
            ->add('users')
            ->add('organizer')
            ->add('site',EntityType::class,[
                'class' => Site::class,
            ])
            ->add('city',EntityType::class,[
                'class' => City::class,
                'placeholder' => '',
                'mapped' => false,
                'required' => false,
            ])
            ->add('newCity',TextType::class,[
                    'mapped' => false,
                    'label' => 'New city',
                    'required' => false,
                ])
//             ->add('place',EntityType::class,[
//                 'class' => Place::class,
//                 'placeholder' => '',
//                 'empty_data' => '',
//             ])
                ->add('placeSelect',EntityType::class,[
                    'class' => Place::class,
                    'required' => false,
                    'mapped' => false,
                ])
            ->add('place',PlaceType::class)
            ->add('cityType',CityType::class,[
                'mapped' => false,
                'required' => false,
                'attr' => ['readonly' => true]
            ])
//            ->add('placeType', PlaceType::class,[
//                'mapped' => false,
//                'required' => false,
//                'attr' => ['readonly' => true]
//            ])
            ->add('save',SubmitType::class,[
                'attr' => ['class' => 'btn btn-lg btn-primary'],
                'label' => 'Save'
            ])
            ->add('publish',SubmitType::class,[
                'attr' => ['class' => 'btn btn-lg btn-primary'],
                'label' => 'Publish'
            ])
        ;
        $builder->get('placeSelect')->resetViewTransformers();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
        ]);
    }
}
