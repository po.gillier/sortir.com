<?php

namespace App\Form;

use App\Entity\Place;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlaceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,[
                'attr' => ['readonly' => true],
                'empty_data' => ''
            ])
            ->add('street',TextType::class,[
                'attr' => ['readonly' => true],
                'empty_data' => ''
            ])
            ->add('latitude',NumberType::class,[
                'attr' => ['readonly' => true],
                'empty_data' => '0'
            ])
            ->add('longitude',NumberType::class,[
                'attr' => ['readonly' => true],
                'empty_data' => '0'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Place::class,
        ]);
    }
}
